-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : ven. 30 avr. 2021 à 10:41
-- Version du serveur :  5.7.31
-- Version de PHP : 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `cvweb4`
--

-- --------------------------------------------------------

--
-- Structure de la table `activites`
--

DROP TABLE IF EXISTS `activites`;
CREATE TABLE IF NOT EXISTS `activites` (
  `id_activ` int(11) NOT NULL AUTO_INCREMENT,
  `nom_activ` text NOT NULL,
  `niv_activ` int(11) NOT NULL,
  PRIMARY KEY (`id_activ`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `activites`
--

INSERT INTO `activites` (`id_activ`, `nom_activ`, `niv_activ`) VALUES
(30, 'Travail en équipe', 2),
(31, 'Assiduité', 3);

-- --------------------------------------------------------

--
-- Structure de la table `autres_activite`
--

DROP TABLE IF EXISTS `autres_activite`;
CREATE TABLE IF NOT EXISTS `autres_activite` (
  `id_autr_activ` int(11) NOT NULL AUTO_INCREMENT,
  `nom_activ` varchar(255) NOT NULL,
  `niveau_activite` int(11) NOT NULL,
  PRIMARY KEY (`id_autr_activ`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `competence`
--

DROP TABLE IF EXISTS `competence`;
CREATE TABLE IF NOT EXISTS `competence` (
  `id_comp` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `niveau` int(11) NOT NULL,
  PRIMARY KEY (`id_comp`)
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `competence`
--

INSERT INTO `competence` (`id_comp`, `description`, `niveau`) VALUES
(40, 'Langage C', 0),
(41, 'PHP', 1),
(43, 'HTML/CSS/BOOTSTRAP', 2),
(44, 'Angular JS', 1);

-- --------------------------------------------------------

--
-- Structure de la table `diplome`
--

DROP TABLE IF EXISTS `diplome`;
CREATE TABLE IF NOT EXISTS `diplome` (
  `id_educ` int(11) NOT NULL AUTO_INCREMENT,
  `date_debut` date NOT NULL,
  `date_fin` date NOT NULL,
  `etablissement` varchar(255) NOT NULL,
  `diplome_obtenue` varchar(255) NOT NULL,
  PRIMARY KEY (`id_educ`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `diplome`
--

INSERT INTO `diplome` (`id_educ`, `date_debut`, `date_fin`, `etablissement`, `diplome_obtenue`) VALUES
(35, '2014-09-08', '2016-07-08', 'Collège Catholique Saint Michel', 'BEPC'),
(36, '2017-09-07', '2018-07-31', 'Collège Catholique Saint Michel', 'Baccaleaureat'),
(39, '2020-10-01', '2021-06-30', 'Université ESGIS Cotonou', 'LICENCE');

-- --------------------------------------------------------

--
-- Structure de la table `expericence_pro`
--

DROP TABLE IF EXISTS `expericence_pro`;
CREATE TABLE IF NOT EXISTS `expericence_pro` (
  `id_experience` int(11) NOT NULL AUTO_INCREMENT,
  `date_debut` date NOT NULL,
  `date_fin` date NOT NULL,
  `description` text NOT NULL,
  `employeur` varchar(255) NOT NULL,
  PRIMARY KEY (`id_experience`)
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `expericence_pro`
--

INSERT INTO `expericence_pro` (`id_experience`, `date_debut`, `date_fin`, `description`, `employeur`) VALUES
(45, '2020-03-06', '2021-06-30', 'YOD Ingénierie est une société de Services en Ingénierie Informatique (SSII). Nous proposons des solutions de gestions adaptées à toutes les tailles d’entreprises (petit commerce, PME, PMI, entrepreneur) pour tous secteurs d’activités.\r\n\r\nYOD Ingénierie offre un panel de compétences pour tous les besoins en gestion informatique et numérique de nos partenaires éditeurs de logiciels, banque, finance, assurance, transport ou de la santé\r\n\r\n', 'YOD INGIENERIE'),
(46, '2019-03-06', '2020-04-24', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'SOBEBRA'),
(47, '2018-05-10', '2019-04-03', 'La Communauté Electrique du Bénin - CEB est une organisation internationale détenue conjointement par les gouvernements du Bénin et du Togo. Il est en charge du développement des infrastructures électriques dans les deux pays qui dépendent fortement des importations énergétiques du Ghana.', 'Communauté élèctrique du Bénin');

-- --------------------------------------------------------

--
-- Structure de la table `identite_perso`
--

DROP TABLE IF EXISTS `identite_perso`;
CREATE TABLE IF NOT EXISTS `identite_perso` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `profil` varchar(50) NOT NULL,
  `titre_pro` text NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `numero_telephone` varchar(255) NOT NULL,
  `situation_matrimoniale` int(11) NOT NULL,
  `age` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `adresse` varchar(255) NOT NULL,
  `resume` text NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `identite_perso`
--

INSERT INTO `identite_perso` (`id_user`, `profil`, `titre_pro`, `nom`, `prenom`, `numero_telephone`, `situation_matrimoniale`, `age`, `email`, `adresse`, `resume`) VALUES
(24, 'images/1619775239.jpg', 'Ingenieur informaticienne', 'APOVO', 'Richbell Fanneth Biowa', '98446412', 1, 19, 'apovorichbell2018@gmail.com', 'Cotonou, Benin', 'Etudiante en année de LICENCE à ESGIS en IRT(Informatique Réseaux et Télé communication) spécialement architecture logicielle. ');

-- --------------------------------------------------------

--
-- Structure de la table `langue`
--

DROP TABLE IF EXISTS `langue`;
CREATE TABLE IF NOT EXISTS `langue` (
  `id_lang` int(11) NOT NULL AUTO_INCREMENT,
  `denomiation` varchar(255) NOT NULL,
  `niveau` int(11) NOT NULL,
  PRIMARY KEY (`id_lang`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `langue`
--

INSERT INTO `langue` (`id_lang`, `denomiation`, `niveau`) VALUES
(26, 'Francais', 2),
(29, 'Anglais ', 0),
(28, 'Fon-gbé', 1);

-- --------------------------------------------------------

--
-- Structure de la table `sport`
--

DROP TABLE IF EXISTS `sport`;
CREATE TABLE IF NOT EXISTS `sport` (
  `id_sport` int(11) NOT NULL AUTO_INCREMENT,
  `nom_sport` varchar(255) NOT NULL,
  `niveau` int(11) NOT NULL,
  PRIMARY KEY (`id_sport`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `sport`
--

INSERT INTO `sport` (`id_sport`, `nom_sport`, `niveau`) VALUES
(24, 'Tennis', 1),
(25, 'Course', 2);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
