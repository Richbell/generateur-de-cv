<?php
  require_once("connDB.php");

  // identite_perso

    $req1 = "SELECT * FROM identite_perso";
    $resultat1=$pdo->prepare($req1);
    $resultat1->execute();
    $row1 = $resultat1->fetchAll(PDO::FETCH_ASSOC);

   if (!empty($row1)) {
      foreach ($row1 as $value1) {
        $user_id = $value1['id_user'];
        $profil = $value1['profil'];
        $titre_pro = $value1['titre_pro'];
        $nom = $value1['nom'];
        $prenom = $value1['prenom'];
        $tel = $value1['numero_telephone'];
        $situation = $value1['situation_matrimoniale'];
        $age = $value1['age'];
        $email = $value1['email'];
        $adresse = $value1['adresse'];
        $resume = $value1['resume'];
      }
   }else{
      $user_id = '';
      $profil = '';
      $titre_pro = '';
      $nom = '';
      $prenom = '';
      $tel = '';
      $situation = '';
      $age = '';
      $email = '';
      $adresse = '';
      $resume = '';
   }
      

    // expericence_pro

    $req2 = "SELECT * FROM expericence_pro";
    $resultat2=$pdo->prepare($req2);
    $resultat2->execute();
    $row2 = $resultat2->fetchAll(PDO::FETCH_ASSOC);

    $req2_1 = "SELECT COUNT(*) FROM expericence_pro";
    $resultat2_1=$pdo->prepare($req2_1);
    $resultat2_1->execute();
    $row2_1 = $resultat2_1->fetchColumn();

    // diplome

    $req3 = "SELECT * FROM diplome";
    $resultat3=$pdo->prepare($req3);
    $resultat3->execute();
    $row3 = $resultat3->fetchAll(PDO::FETCH_ASSOC);

    $req3_1 = "SELECT COUNT(*) FROM diplome";
    $resultat3_1=$pdo->prepare($req3_1);
    $resultat3_1->execute();
    $row3_1 = $resultat3_1->fetchColumn();

    // competence

    $req4 = "SELECT * FROM competence";
    $resultat4=$pdo->prepare($req4);
    $resultat4->execute();
    $row4 = $resultat4->fetchAll(PDO::FETCH_ASSOC);

    $req4_1 = "SELECT COUNT(*) FROM competence";
    $resultat4_1=$pdo->prepare($req4_1);
    $resultat4_1->execute();
    $row4_1 = $resultat4_1->fetchColumn();

    // langue

    $req5 = "SELECT * FROM langue";
    $resultat5=$pdo->prepare($req5);
    $resultat5->execute();
    $row5 = $resultat5->fetchAll(PDO::FETCH_ASSOC);

    $req5_1 = "SELECT COUNT(*) FROM langue";
    $resultat5_1=$pdo->prepare($req5_1);
    $resultat5_1->execute();
    $row5_1 = $resultat5_1->fetchColumn();

    // sport

    $req6 = "SELECT * FROM sport";
    $resultat6=$pdo->prepare($req6);
    $resultat6->execute();
    $row6 = $resultat6->fetchAll(PDO::FETCH_ASSOC);

    $req6_1 = "SELECT COUNT(*) FROM sport";
    $resultat6_1=$pdo->prepare($req6_1);
    $resultat6_1->execute();
    $row6_1 = $resultat6_1->fetchColumn();

    // activites

    $req7 = "SELECT * FROM activites";
    $resultat7=$pdo->prepare($req7);
    $resultat7->execute();
    $row7 = $resultat7->fetchAll(PDO::FETCH_ASSOC);

    $req7_1 = "SELECT COUNT(*) FROM activites";
    $resultat7_1=$pdo->prepare($req7_1);
    $resultat7_1->execute();
    $row7_1 = $resultat7_1->fetchColumn();

  $cssid = rand();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>

  <title>CV de <?php echo $nom." ".$prenom ?></title>
 
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <meta name="keywords" content=""/>
  <meta http-equiv="content-language" content="fr"/>
  <meta name="DC.Language" scheme="NISOZ39.50" content="fr"/>

  <meta name="title" content="CV de <?php echo $nom." ".$prenom ?>"/>
  <meta name="description" content="<?php echo $resume ?>"/>
  <meta name="image" content="http://moncvweb.ml/<?php echo $profil ?>"/>
  <meta name="url" content="http://moncvweb.ml/index.php"/>

  <meta property="og:title" content="CV de <?php echo $nom." ".$prenom ?>"/>
  <meta property="og:description" content="<?php echo $resume ?>"/>
  <meta property="og:image" content="http://moncvweb.ml/<?php echo $profil ?>"/>
  <meta property="og:url" content="http://moncvweb.ml/index.php"/>

  <meta name="twitter:title" content="CV de <?php echo $nom." ".$prenom ?>"/>
  <meta name="twitter:description" content="<?php echo $resume ?>"/>
  <meta name="twitter:image" content="http://moncvweb.ml/<?php echo $profil ?>"/>
  <meta name="twitter:card" content="summary_large_image"/>

  <link rel="icon" href="images/favicon.png" />
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@700&display=swap" rel="stylesheet"> 
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/style.css?id=<?php echo $cssid ?>"> 
  <link rel="stylesheet" href="css/font-awesome-animation.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="js/scrollreveal.min.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.debug.js"></script>
</head>
<div style="position: fixed; width: 100%; top: 0; padding-right: 30px; z-index: 999; text-align: right" align="right">
  <button class="btn btn-default" onclick="download();" style="border-radius: 0px 0px 10px 10px; background: #de4d4a; color: #fff; border: none; box-shadow: 0 4px 18px 0 rgba(0, 0, 0, 0.3); "><i class="fas fa-download"></i> Telecharger le CV</button>
</div>
<body style="background: #ccd0d1">

  <div class="container mob1" id="cvcont" style="padding: 0">
    <div id="content">
    <div class="col-lg-12">
      <div class="col-lg-4" align="center" style="padding-top: 50px">
        <img src="<?php echo $profil ?>" class="img-responsive img-circle" style="max-height: 200px">
        <h2 id="lenom" style="font-weight: bold; line-height: 1.5em"><?php echo $nom ?> <font color="#de4d4a"><?php echo $prenom ?></font><br><small style="font-size: 14px; font-weight: bold"><?php echo $titre_pro ?></small></h2>
      </div>
      <div class="col-lg-8 mob3" style="padding: 0; padding-top: 50px;">
        <div class="col-lg-10 col-lg-offset-1" style="padding: 0;">
          <h3 class="mob2" style="font-weight: bold; color: #666">RÉSUMÉ</h3>
          <p style="font-size: 15px; text-align: justify; min-height: 200px"><?php echo $resume ?></p>
          <hr class="mob5" style="border: none; border-top: solid 3px #888; margin-top: 30px">
        </div>
      </div>
    </div>
    <div class="col-lg-12" style="padding: 0">
      <div class="col-lg-4 col-xs-12 mob4" style="min-height: 500px; border-radius: 0px 130px 0px 0px; background: #10183d; padding-bottom: 60px">

        <div class="col-lg-12 col-xs-12" style="padding: 0">
          <h4 id="titre1" style="margin-top: 60px">CONTACT</h4>
        </div>
        <div class="col-lg-10 col-lg-offset-1 col-xs-12" style="margin-top: 15px">
          <span class="badge" id="badgeType1"><i class="fas fa-phone"></i></span>
          <h5 style="color: #fff; padding-left: 60px; margin-top: 12px">
            <?php
              $telephone = preg_replace('~.*(\d{2})[^\d]*(\d{2})[^\d]*(\d{2})[^\d]*(\d{2}).*~', '$1 $2 $3 $4', $tel);
              echo $telephone;
            ?>
          </h5>
        </div>
        <div class="col-lg-10 col-lg-offset-1 col-xs-12" style="margin-top: 15px">
          <span class="badge" id="badgeType1"><img src="images/avatar.png" class="img-responsive"></span>
          <h5 style="color: #fff; padding-left: 60px; margin-top: 12px"><?php echo $email ?></h5>
        </div>
        <div class="col-lg-10 col-lg-offset-1 col-xs-12" style="margin-top: 15px">
          <span class="badge" id="badgeType1" style="padding-top: 8px"><i class="fas fa-map-marker-alt"></i></span>
          <h5 style="color: #fff; padding-left: 60px; margin-top: 12px"><?php echo $adresse ?></h5>
        </div>

        <?php  
          if ($row4_1 != 0) {
            ?>
              <div class="col-lg-12 col-xs-12" style="padding: 0">
                <h4 id="titre1" style="margin-top: 70px">COMPÉTENCES</h4>
              </div>
            <?php
            foreach ($row4 as $value4) {
              ?>
                <div class="col-lg-10 col-lg-offset-1 col-xs-12" style="margin-top: 5px">
                  <div class="skill_name">
                    <h5><?php echo $value4['description'] ?></h5>
                  </div>
                  <?php
                    switch ($value4['niveau']) {
                      case 0:
                        $per = "50%";
                        break;

                        case 1:
                        $per = "65%";
                        break;

                        case 2:
                        $per = "75%";
                        break;

                        case 3:
                        $per = "90%";
                        break;
                      
                      default:
                        # code...
                        break;
                    }
                  ?>
                  <div class="skill_progress">
                    <span style="width: <?php echo $per ?>;"></span>
                  </div>
                  <h6 style="color: #eee; margin: 0; margin-top: 5px; float: right; padding-right: 8px; font-weight: bold"><?php echo $per ?></h6>
                </div>
              <?php
            }
          }else{
            // rien
          }
        ?>

        <?php
          if ($row5_1 != 0) {
            ?>
               <div class="col-lg-12 col-xs-12" style="padding: 0">
                  <h4 id="titre1" style="margin-top: 70px">LANGUES</h4>
                </div>
            <?php
            foreach ($row5 as $value5) {
              ?>
                <div class="col-lg-10 col-lg-offset-1 col-xs-12" style="margin-top: 5px">
                  <div class="skill_name">
                    <h5><?php echo $value5['denomiation'] ?></h5>
                  </div>
                  <?php
                    switch ($value5['niveau']) {
                      case 0:
                        $per = "50%";
                        break;

                        case 1:
                        $per = "65%";
                        break;

                        case 2:
                        $per = "75%";
                        break;

                        case 3:
                        $per = "90%";
                        break;
                      
                      default:
                        # code...
                        break;
                    }
                  ?>
                  <div class="skill_progress">
                    <span style="width: <?php echo $per ?>;"></span>
                  </div>
                  <h6 style="color: #fff; margin: 0; margin-top: 5px; float: right; padding-right: 8px"><?php echo $per ?></h6>
                </div>
              <?php
            }
          }else{
            // rien
          }
        ?>

        <?php
          if ($row6_1 != 0) {
            ?>
              <div class="col-lg-12 col-xs-12" style="padding: 0">
                <h4 id="titre1" style="margin-top: 70px">SPORTS</h4>
              </div>
            <?php
            foreach ($row6 as $value6) {
              ?>
                <div class="col-lg-10 col-lg-offset-1 col-xs-12" style="margin-top: 5px">
                  <div class="skill_name">
                    <h5><?php echo $value6['nom_sport'] ?></h5>
                  </div>
                  <?php
                    switch ($value6['niveau']) {
                      case 0:
                        $per = "50%";
                        break;

                        case 1:
                        $per = "65%";
                        break;

                        case 2:
                        $per = "75%";
                        break;

                        case 3:
                        $per = "90%";
                        break;
                      
                      default:
                        # code...
                        break;
                    }
                  ?>
                  <div class="skill_progress">
                    <span style="width: <?php echo $per ?>;"></span>
                  </div>
                  <h6 style="color: #fff; margin: 0; margin-top: 5px; float: right; padding-right: 8px"><?php echo $per ?></h6>
                </div>
              <?php
            }
          }else{
            // rien
          }
        ?>
        

        <!--<div class="col-lg-12" style="padding: 0">
          <h4 id="titre1" style="margin-top: 70px">AUTRES ACTIVITÉS</h4>
        </div>
        <div class="col-lg-10 col-lg-offset-1" style="margin-top: 5px">
          <div class="skill_name">
            <h5>Autre activité 1</h5>
          </div>
          <div class="skill_progress">
            <span style="width: 60%;"></span>
          </div>
          <h6 style="color: #fff; margin: 0; margin-top: 5px; float: right; padding-right: 8px">50%</h6>
        </div>
        <div class="col-lg-10 col-lg-offset-1" style="margin-top: 5px">
          <div class="skill_name">
            <h5>Autre activité 2</h5>
          </div>
          <div class="skill_progress">
            <span style="width: 60%;"></span>
          </div>
          <h6 style="color: #fff; margin: 0; margin-top: 5px; float: right; padding-right: 8px">50%</h6>
        </div>
        <div class="col-lg-10 col-lg-offset-1" style="margin-top: 5px">
          <div class="skill_name">
            <h5>Autre activité 3</h5>
          </div>
          <div class="skill_progress">
            <span style="width: 60%;"></span>
          </div>
          <h6 style="color: #fff; margin: 0; margin-top: 5px; float: right; padding-right: 8px">50%</h6>
        </div>-->

      </div>

      <div class="col-lg-8 col-xs-12 mob6" style="padding: 0;">
        <div class="col-lg-10 col-lg-offset-1 col-xs-12" style="padding: 0">
          <?php
            if ($row3_1 != 0) {
              ?>
                <div class="col-lg-12 col-xs-12" style="padding: 0;">
                  <h4 id="titre1" style="margin-top: 60px; box-shadow: none; text-align: left; padding-left: 80px; margin-left: 0; width: 100%"><i class="fas fa-graduation-cap" style="position: relative; font-size: 35px; margin-top: -7px"></i> DIPLÔMES</h4>
                </div>
                <div class="col-lg-12 col-xs-12" style="margin-top: 15px">
                  <span class="badge" id="badgeType3"> </span>
              <?php
              foreach ($row3 as $value3) {
                ?>
                  <div class="col-lg-12 col-xs-12" style="padding: 0; border-left: solid 2px #bbb; padding-bottom: 50px">
                    <div class="col-lg-1 col-xs-2" style="padding: 0;">
                      <span class="badge" id="badgeType2" style="margin-top: 0px; margin-left: -9px"> </span>
                    </div>
                    <div class="col-lg-3 col-xs-10" style="margin-left: -30px">
                      <?php
                        $startyr = DateTime::createFromFormat('Y-m-d', $value3['date_debut'] )->format('Y');
                        $endyr = DateTime::createFromFormat('Y-m-d', $value3['date_fin'] )->format('Y');
                      ?>
                      <h5 style="margin-top: 0px; font-weight: bold"><?php echo $startyr." - ".$endyr ?></h5>
                    </div>
                    <div class="col-lg-8 col-lg-offset-0 col-xs-11 col-xs-offset-1">
                      <h4 style="color: #de4d4a; margin-top: 0px"><?php echo $value3['diplome_obtenue'] ?></h4>
                      <h5><?php echo $value3['etablissement'] ?></h5>
                    </div>
                  </div>
                <?php
              }
              ?>
                </div>
              <?php
            }else{
              // rien
            }
          ?>
          
        </div>

        <div class="col-lg-10 col-lg-offset-1 col-xs-12" style="padding: 0">
          <?php
            if ($row2_1 != 0) {
              ?>
                <div class="col-lg-12 col-xs-12" style="padding: 0">
                  <h4 id="titre1" style="margin-top: 60px; box-shadow: none; text-align: left; padding-left: 80px; margin-left: 0; width: 100%"><i class="fas fa-briefcase" style="position: relative; font-size: 35px; margin-top:-7px"></i> EXPÉRIENCES</h4>
                </div>
                <div class="col-lg-12 col-xs-12" style="margin-top: 15px">
                  <span class="badge" id="badgeType3"> </span>
              <?php
              foreach ($row2 as $value2) {
                ?>
                  <div class="col-lg-12 col-xs-12" style="padding: 0; border-left: solid 2px #bbb; padding-bottom: 50px">
                    <div class="col-lg-1 col-xs-2" style="padding: 0;">
                      <span class="badge" id="badgeType2" style="margin-top: 0px; margin-left: -9px"> </span>
                    </div>
                    <div class="col-lg-3 col-xs-10" style="margin-left: -30px">
                      <?php
                        $startyr = DateTime::createFromFormat('Y-m-d', $value2['date_debut'] )->format('Y');
                        $endyr = DateTime::createFromFormat('Y-m-d', $value2['date_fin'] )->format('Y');
                      ?>
                      <h5 style="margin-top: 0px; font-weight: bold"><?php echo $startyr." - ".$endyr ?></h5>
                    </div>
                    <div class="col-lg-8 col-lg-offset-0 col-xs-11 col-xs-offset-1 mob7" style="width: 70%; padding-right: 0">
                      <h4 style="color: #de4d4a; margin-top: 0px"><?php echo $value2['employeur'] ?></h4>
                      <p style="font-size: 15px"><?php echo $value2['description'] ?></p>
                    </div>
                  </div>
                <?php
              }
              ?>
                </div>
              <?php
            }else{
              // rien
            }
          ?>
          
          
        </div>

      </div>

    </div>
  </div>
</div>
</body>
 <!--content end-->
  <script type="text/javascript">

function download(){
  var pdf = new jsPDF('p','pt','a4');
  pdf.addHTML($('#cvcont'),function() {
      pdf.save('web.pdf');
  });
}
</script>

</html>