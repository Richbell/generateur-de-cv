<?php
  require_once("connDB.php");

  $req0 = "SELECT COUNT(*) FROM identite_perso";
  $resultat0=$pdo->prepare($req0);
  $resultat0->execute();
  $row0 = $resultat0->fetchColumn();

  $error_img = isset($_GET['error_img'])?$_GET['error_img']:'';

    // identite_perso

    $req1 = "SELECT * FROM identite_perso";
    $resultat1=$pdo->prepare($req1);
    $resultat1->execute();
    $row1 = $resultat1->fetchAll(PDO::FETCH_ASSOC);

   if (!empty($row1)) {
      foreach ($row1 as $value1) {
        $user_id = $value1['id_user'];
        $profil = $value1['profil'];
        $titre_pro = $value1['titre_pro'];
        $nom = $value1['nom'];
        $prenom = $value1['prenom'];
        $tel = $value1['numero_telephone'];
        $situation = $value1['situation_matrimoniale'];
        $age = $value1['age'];
        $email = $value1['email'];
        $adresse = $value1['adresse'];
        $resume = $value1['resume'];
      }
   }else{
      $user_id = '';
      $profil = '';
      $titre_pro = '';
      $nom = '';
      $prenom = '';
      $tel = '';
      $situation = '';
      $age = '';
      $email = '';
      $adresse = '';
      $resume = '';
   }
      

    // expericence_pro

    $req2 = "SELECT * FROM expericence_pro";
    $resultat2=$pdo->prepare($req2);
    $resultat2->execute();
    $row2 = $resultat2->fetchAll(PDO::FETCH_ASSOC);

    // diplome

    $req3 = "SELECT * FROM diplome";
    $resultat3=$pdo->prepare($req3);
    $resultat3->execute();
    $row3 = $resultat3->fetchAll(PDO::FETCH_ASSOC);

    // competence

    $req4 = "SELECT * FROM competence";
    $resultat4=$pdo->prepare($req4);
    $resultat4->execute();
    $row4 = $resultat4->fetchAll(PDO::FETCH_ASSOC);

    // langue

    $req5 = "SELECT * FROM langue";
    $resultat5=$pdo->prepare($req5);
    $resultat5->execute();
    $row5 = $resultat5->fetchAll(PDO::FETCH_ASSOC);

    // sport

    $req6 = "SELECT * FROM sport";
    $resultat6=$pdo->prepare($req6);
    $resultat6->execute();
    $row6 = $resultat6->fetchAll(PDO::FETCH_ASSOC);

    // activites

    $req7 = "SELECT * FROM activites";
    $resultat7=$pdo->prepare($req7);
    $resultat7->execute();
    $row7 = $resultat7->fetchAll(PDO::FETCH_ASSOC);

 

  $cssid = rand();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>

  <title>Formulaire du CV</title>
 
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <meta name="keywords" content=""/>
  <meta http-equiv="content-language" content="fr"/>
  <meta name="DC.Language" scheme="NISOZ39.50" content="fr"/>

  <meta name="title" content=""/>
  <meta name="description" content=""/>
  <meta name="image" content=""/>
  <meta name="url" content=""/>

  <meta property="og:title" content=""/>
  <meta property="og:description" content=""/>
  <meta property="og:image" content=""/>
  <meta property="og:url" content=""/>

  <meta name="twitter:title" content=""/>
  <meta name="twitter:description" content=""/>
  <meta name="twitter:image" content=""/>
  <meta name="twitter:card" content="summary_large_image"/>

  <link rel="icon" href="images/favicon.png" />
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@700&display=swap" rel="stylesheet"> 
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/style.css?id=<?php echo $cssid ?>"> 
  <link rel="stylesheet" href="css/font-awesome-animation.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="js/scrollreveal.min.js" type="text/javascript"></script>

  <style type="text/css">
    div{
      
    }
  </style>
  
</head>

<body style="background: #eee">
  <div class="container mob8" style="background: #fff; border-radius: 10px; box-shadow: 0 4px 18px 0 rgba(0, 0, 0, 0.1); min-height: 500px; margin-top: 50px; padding-bottom: 50px; margin-bottom: 50px">
    <div class="col-lg-10 col-lg-offset-1" style="padding-top: 30px;">
      <div class="col-lg-12">
        <h2 class="mob9" style="font-weight: bold; color: #666; display: inline-block">Formulaire du CV</h2>
        <span class="pull-right mob10" style="margin: 0; padding: 0; padding-top: 22px;">
          <a href="index.php" target="_blank"><button class="btn btn-default btn-success"><i class="fas fa-eye"></i> Apercu</button></a>
          <a href="delete_all.php"><button class="btn btn-default btn-danger"><i class="fas fa-eraser"></i> Tout effacer</button></a>
        </span>
        <hr class="mob11" style="border: none; border-top: solid 1px #aaa;">
      </div>
      <form role="form" name="myform" id="myForm" method="POST" action="save.php" enctype="multipart/form-data">
        <input type="hidden" name="exist_idp" value="<?php echo $user_id ?>">
        <div class="form-group col-lg-12 mob12" style="margin-top: 15px;">
          <h4 style="font-weight: bold; border-left: solid 5px #00b0f0; padding-left: 10px; color: #00b0f0">INDENTITE PERSONELLE</h4>
        </div>
        <?php
          if ($error_img == 1) {
            ?>
            <div class="col-lg-12">
              <div class="alert alert-danger fade in" style="margin-bottom: 20px">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong><i class="fas fa-exclamation-triangle"></i></strong> &nbsp;Erreur: Cette image n'est pas pris en charge. 
              </div>
            </div>
            <?php
          }elseif ($error_img == 2) {
            ?>
            <div class="col-lg-12">
              <div class="alert alert-danger fade in" style="margin-bottom: 20px">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong><i class="fas fa-exclamation-triangle"></i></strong> &nbsp;Erreur: Seuls les fichiers JPG, JPEG, PNG et GIF sont acceptés.
              </div>
            </div>
            <?php
          }
        ?>
        <div class="col-lg-12" style="padding: 0; margin-top: 15px; margin-bottom: 20px;">
          <div class="form-group col-lg-3 mob13" align="">
                <input type="hidden" name="oldprofil" value="<?php echo $profil ?>">
                <span class="badge" id="badgeTypeAvatar" style="background-image: url(<?php echo $profil ?>), url(images/avatar.png); background-size: cover; background-position: center; width: 180px; height: 180px; border: solid 1px #ccc;"> </span>
                <label class="btn btn-default pushlink" id="boutonPhoto" for="fileToUpload" required="required" style="padding-top: 15px"><i class="fas fa-camera"></i></label>
                <input class="fileToUpload" type="file" accept="image/*" name="fileToUpload" id="fileToUpload" required="required" style="opacity: 0; position: absolute; z-index: -1">
                <center><h6 style="color: #f00">NB: image | ratio 1:1</h6></center>
          </div>
              <script type="text/javascript">
                $('#fileToUpload').change(function () {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onloadend = function () {
                       $('#badgeTypeAvatar').css('background-image', 'url("' + reader.result + '")');
                    }
                    if (file) {
                        reader.readAsDataURL(file);
                    } else {
                    }
                });
              </script>
          <div class="form-group col-lg-9" style="">
            <input class="form-control input-lg" type="text" name="stitre_pro" placeholder="Titre professionel *" value="<?php echo $titre_pro ?>" style="margin-top: 5px">
            <textarea class="form-control input-lg" name="sresume" placeholder="Résumé *" style="padding-top: 15px; margin-top: 35px"><?php echo $resume ?></textarea>
          </div>
        </div>
        <div class="form-group col-lg-6">
          <input class="form-control input-lg" type="text" name="snom" placeholder="Nom *" value="<?php echo $nom ?>">
        </div>
        <div class="form-group col-lg-6">
          <input class="form-control input-lg" type="text" name="sprenom" placeholder="Prenom *" value="<?php echo $prenom ?>">
        </div>
        <div class="form-group col-lg-4" style="margin-top: 20px">
          <input class="form-control input-lg" type="tel"  minlength="4" maxlength="16" pattern="([0-9])+" name="stel" placeholder="Telephone *" value="<?php echo $tel ?>">
        </div>
        <div class="form-group col-lg-4" style="margin-top: 20px">
          <select class="form-control input-lg" name="ssituation">
            

            <?php
              switch ($situation) {
                case 1:
                    ?>
                      <option disabled>Situation matrimoniale</option>
                      <option selected value="1">Celibataire sans enfant</option>
                      <option value="2">Celibataire avec enfant</option>
                      <option value="3">Marie</option>
                      <option value="4">Divorce</option>
                    <?php
                  break;

                  case 2:
                    ?>
                      <option disabled>Situation matrimoniale</option>
                      <option value="1">Celibataire sans enfant</option>
                      <option selected value="2">Celibataire avec enfant</option>
                      <option value="3">Marie</option>
                      <option value="4">Divorce</option>
                    <?php
                  break;

                  case 3:
                    ?>
                    <option disabled>Situation matrimoniale</option>
                      <option value="1">Celibataire sans enfant</option>
                      <option value="2">Celibataire avec enfant</option>
                      <option selected value="3">Marie</option>
                      <option value="4">Divorce</option>
                    <?php
                  break;

                  case 4:
                    ?>
                    <option disabled>Situation matrimoniale</option>
                      <option value="1">Celibataire sans enfant</option>
                      <option value="2">Celibataire avec enfant</option>
                      <option value="3">Marie</option>
                      <option selected value="4">Divorce</option>
                    <?php
                  break;
                
                default:
                    ?>
                      <option selected disabled>Situation matrimoniale</option>
                      <option value="1">Celibataire sans enfant</option>
                      <option value="2">Celibataire avec enfant</option>
                      <option value="3">Marie</option>
                      <option value="4">Divorce</option>
                    <?php
                  break;
              }
            ?>
          </select>
        </div>
        <div class="form-group col-lg-4" style="margin-top: 20px">
          <input class="form-control input-lg" type="number" name="sage" min="18" placeholder="Age" value="<?php echo $age ?>">
        </div>
        
        <div class="form-group col-lg-6" style="margin-top: 20px">
          <input class="form-control input-lg" type="email" name="smail" placeholder="E-mail" value="<?php echo $email ?>">
        </div>
        <div class="form-group col-lg-6" style="margin-top: 20px">
          <input class="form-control input-lg" type="text" name="sadresse" placeholder="Adresse" value="<?php echo $adresse ?>">
        </div>
        <div class="form-group col-lg-12 mob14" style="margin-top: 30px">
          <h4 style="font-weight: bold; border-left: solid 5px #00b0f0; padding-left: 10px; color: #00b0f0">EXPERIENCE PROFESSIONELLE</h4>
        </div>

        <div class="col-lg-12 mob15" id="myshowdiv1" style="margin-top: -50px; padding: 0;">
          <?php
            if (empty($row2)) {
              ?>
                <div class="form-group col-lg-12 col-xs-12" style="margin-top: 50px">
                  <textarea class="form-control input-lg expdescboxold" name="sdescexpold1" id="descexpold1" placeholder="Description du poste" style="padding-top: 15px"></textarea>
                </div>
                <div class="col-lg-3">
                  <input type="date" class="form-control input-lg expdatedebboxold" name="sdatedebutexpold1" id="datedebutexpold1" style="margin-top: 20px; padding: 0; padding-left: 10px; padding-right: 10px; font-size: 13px" placeholder="Date début">
                </div>
                <div class="col-lg-3">
                  <input type="date" class="form-control input-lg expdatefinboxold" name="sdatefinexpold1" id="datefinexpold1" style="margin-top: 20px; padding: 0; padding-left: 10px; padding-right: 10px; font-size: 13px" placeholder="Date fin" > 
                </div>
                <div class="col-lg-6">
                  <input type="text" class="form-control input-lg expempboxold" name="semployeurold1" id="employeurold1" style="margin-top: 20px" placeholder="Nom de l'employeur">
                </div>
                <input type="hidden" id="comptexpold" name="sexpcompteold" value="1">
              <?php
            }else{
              $i = 0;
              foreach ($row2 as $value2) {
                $i = $i + 1;
              ?>
                <input type="hidden" name="exist_exp<?php echo $i ?>" value="<?php echo $value2['id_experience'] ?>">
                <a href="delete_field.php?id_field=<?php echo $value2['id_experience'] ?>&table=expericence_pro"><span class="badge pull-right mob16" style="margin-top: 50px; margin-bottom: 10px; font-weight: normal; padding: 10px; margin-right: 15px; border-radius: 30px;">
                  <i class="fas fa-trash-alt"></i> Supprimer
                </span></a>
                <div class="form-group col-lg-12 col-xs-12">
                  <textarea class="form-control input-lg expdescboxold" name="sdescexpold<?php echo $i ?>" id="descexpold<?php echo $i ?>" placeholder="Description du poste" style="padding-top: 15px"><?php echo $value2['description'] ?></textarea>
                </div>
                <div class="col-lg-3">
                  <input type="date" class="form-control input-lg expdatedebboxold" name="sdatedebutexpold<?php echo $i ?>" id="datedebutexpold<?php echo $i ?>" style="margin-top: 20px; padding: 0; padding-left: 10px; padding-right: 10px; font-size: 13px" placeholder="Date début" value="<?php echo $value2['date_debut'] ?>">
                </div>
                <div class="col-lg-3">
                  <input type="date" class="form-control input-lg expdatefinboxold" name="sdatefinexpold<?php echo $i ?>" id="datefinexpold<?php echo $i ?>" style="margin-top: 20px; padding: 0; padding-left: 10px; padding-right: 10px; font-size: 13px" placeholder="Date fin" value="<?php echo $value2['date_fin'] ?>"> 
                </div>
                <div class="col-lg-6">
                  <input type="text" class="form-control input-lg expempboxold" name="semployeurold<?php echo $i ?>" id="employeurold<?php echo $i ?>" style="margin-top: 20px" placeholder="Nom de l'employeur" value="<?php echo $value2['employeur'] ?>">
                </div>
              <?php
            }
            ?>
              <input type="hidden" id="comptexpold" name="sexpcompteold" value="<?php echo $i ?>">
            <?php
          }

          ?>
          <input type="hidden" id="comptexp" name="sexpcompte" value="0">
        </div>
        

        <script type="text/javascript">
          function addexp() {

            var newElem0 = document.createElement('span');
            newElem0.className = 'badge dimbadge pull-right mob16';
            newElem0.id = 'remexp';
            newElem0.style = "cursor: pointer; font-weight: normal; padding: 10px; margin-top: 50px; margin-bottom: 10px; margin-right: 15px; border-radius: 30px";
            newElem0.setAttribute("onclick", "dimexp(this.id);");
            var text0 = document.createTextNode('\u2A09 Diminuer');
            newElem0.appendChild(text0);
            document.getElementById("myshowdiv1").appendChild(newElem0);
            var dimbadge = document.getElementsByClassName('dimbadge');
            for (var i = 0; i < dimbadge.length; i++) {
                dimbadge[i].id = "remexp" + (i + 1);
            }
            
            var newElem1 = document.createElement('div');
            newElem1.className = 'form-group col-lg-12';
            newElem1.style = 'margin-top: 0px';
            var newElem2 = document.createElement('textarea');
            newElem2.className = 'form-control input-lg expdescbox';
            newElem2.setAttribute("name", "sdescexp");
            newElem2.setAttribute("placeholder", "Description du poste");
            newElem2.style = 'padding-top: 15px';
            newElem1.appendChild(newElem2);
            document.getElementById("myshowdiv1").appendChild(newElem1);

            var newElem3 = document.createElement('div');
            newElem3.className = 'col-lg-3';
            newElem3.style = 'margin-top: 0px';
            var newElem4 = document.createElement('input');
            newElem4.className = 'form-control input-lg expdatedebbox';
            newElem4.setAttribute("type", "date");
            newElem4.setAttribute("name", "sdatedebutexp");
            newElem4.style = 'margin-top: 20px; padding: 0; padding-left: 10px; font-size: 13px';
            newElem3.appendChild(newElem4);
            document.getElementById("myshowdiv1").appendChild(newElem3);

            var newElem5 = document.createElement('div');
            newElem5.className = 'col-lg-3';
            newElem5.style = 'margin-top: 0px';
            var newElem6 = document.createElement('input');
            newElem6.className = 'form-control input-lg expdatefinbox';
            newElem6.setAttribute("type", "date");
            newElem6.setAttribute("name", "sdatefinexp");
            newElem6.style = 'margin-top: 20px; padding: 0; padding-left: 10px; font-size: 13px';
            newElem5.appendChild(newElem6);
            document.getElementById("myshowdiv1").appendChild(newElem5);

            var newElem7 = document.createElement('div');
            newElem7.className = 'col-lg-6';
            newElem7.style = 'margin-top: 0px';
            var newElem8 = document.createElement('input');
            newElem8.className = 'form-control input-lg expempbox';
            newElem8.setAttribute("type", "text");
            newElem8.setAttribute("name", "semployeur");
            newElem8.setAttribute("placeholder", "Nom de l'employeur");
            newElem8.style = 'margin-top: 20px';
            newElem7.appendChild(newElem8);
            document.getElementById("myshowdiv1").appendChild(newElem7);
           
            var expelema = document.getElementsByClassName("expdescbox");
            var expelemb = document.getElementsByClassName("expdatedebbox");
            var expelemc = document.getElementsByClassName("expdatefinbox");
            var expelemd = document.getElementsByClassName("expempbox");

            for (var i = 0; i < expelema.length; i++) {
                expelema[i].name = "sdescexp" + (i + 1);
                expelema[i].id = "descexp" + (i + 1);
                
            }

            for (var i = 0; i < expelemb.length; i++) {
                expelemb[i].name = "sdatedebutexp" + (i + 1);
                expelemb[i].id = "datedebutexp" + (i + 1);
               
            }

            for (var i = 0; i < expelemc.length; i++) {
                expelemc[i].name = "sdatefinexp" + (i + 1);
                expelemc[i].id = "datefinexp" + (i + 1);
               
            }

            for (var i = 0; i < expelemd.length; i++) {
                expelemd[i].name = "semployeur" + (i + 1);
                expelemd[i].id = "employeur" + (i + 1);
                
            }

            document.getElementById("comptexp").value = i;

          }

          function dimexp(divid) {
            var getid = divid.substring(6, 7);
            document.getElementById("descexp" + getid).parentNode.remove();
            document.getElementById("datedebutexp" + getid).parentNode.remove();
            document.getElementById("datefinexp" + getid).parentNode.remove();
            document.getElementById("employeur" + getid).parentNode.remove();
            document.getElementById(divid).remove();
          }

        </script>

        <div class="col-lg-12" align="right">
          <button type="button" class="btn btn-default" id="bouton1" onclick="addexp();" style="margin-top: 20px; background-color: #00b0f0 "><i class="fas fa-plus"></i> Ajouter une experience</button>
        </div>

        
          <div class="form-group col-lg-12 mob14" style="margin-top: 30px">
            <h4 style="font-weight: bold; border-left: solid 5px #00b0f0; padding-left: 10px; color: #00b0f0">DIPLOMES</h4>
          </div>
          <div class="col-lg-12 mob15" id="myshowdiv2" style="padding: 0; margin-top: -50px;">
            <?php
              if (empty($row3)) {
                ?>
                  <div class="form-group col-lg-6" style="margin-top: 50px">
                    <input class="form-control input-lg dipetaboxold" type="text" name="setablissementold1" id="etablissementold1" placeholder="Etablissement *">
                  </div>
                  <div class="form-group col-lg-6" style="margin-top: 50px">
                    <input class="form-control input-lg dipdipboxold" type="text" name="sdiplomeold1" id="diplomeold1" placeholder="Diplome *">
                  </div>
                  <div class="col-lg-6">
                    <input type="date" class="form-control input-lg dipdatedebboxold" name="sdatedebutetuold1" id="datedebutetuold1" style="margin-top: 20px; padding: 0; padding-left: 10px; padding-right: 10px; font-size: 13px">
                  </div>
                  <div class="col-lg-6">
                    <input type="date" class="form-control input-lg dipdatefinboxold" name="sdatefinetuold1" id="datefinetuold1" style="margin-top: 20px; padding: 0; padding-left: 10px; padding-right: 10px; font-size: 13px">
                  </div>
                  <input type="hidden" id="comptdipold" name="sdipcompteold" value="1">
                <?php
              }else{
                $i = 0;
                foreach ($row3 as $value3) {
                  $i = $i + 1;
                  ?>
                  <input type="hidden" name="exist_dip<?php echo $i ?>" value="<?php echo $value3['id_educ'] ?>">
                  <a href="delete_field.php?id_field=<?php echo $value3['id_educ'] ?>&table=diplome"><span class="badge pull-right mob16" style="cursor: pointer; font-weight: normal; padding: 10px; margin-top: 50px; margin-bottom: 10px; margin-right: 15px; border-radius: 30px">
                    <i class="fas fa-trash-alt"></i> Supprimer
                  </span></a>
                  <div class="form-group col-lg-6 mob17" style="margin-top: 92px">
                    <input class="form-control input-lg dipetaboxold" type="text" name="setablissementold<?php echo $i ?>" id="etablissementold<?php echo $i ?>" placeholder="Etablissement *" value="<?php echo $value3['etablissement'] ?>">
                  </div>
                  <div class="form-group col-lg-6" style="">
                    <input class="form-control input-lg dipdipboxold" type="text" name="sdiplomeold<?php echo $i ?>" id="diplomeold<?php echo $i ?>" placeholder="Diplome *" value="<?php echo $value3['diplome_obtenue'] ?>">
                  </div>
                  <div class="col-lg-6">
                    <input type="date" class="form-control input-lg dipdatedebboxold" name="sdatedebutetuold<?php echo $i ?>" id="datedebutetuold<?php echo $i ?>" value="<?php echo $value3['date_debut'] ?>" style="margin-top: 20px; padding: 0; padding-left: 10px; padding-right: 10px; font-size: 13px">
                  </div>
                  <div class="col-lg-6">
                    <input type="date" class="form-control input-lg dipdatefinboxold" name="sdatefinetuold<?php echo $i ?>" id="datefinetuold<?php echo $i ?>" value="<?php echo $value3['date_fin'] ?>" style="margin-top: 20px; padding: 0; padding-left: 10px; padding-right: 10px; font-size: 13px">
                  </div>
                <?php
                }
                ?>
                  <input type="hidden" id="comptdipold" name="sdipcompteold" value="<?php echo $i ?>">
                <?php
              }
            ?>
          <input type="hidden" id="comptdip" name="sdipcompte" value="0">
        </div>

        <script type="text/javascript">
          function adddip() {

            var newElem0 = document.createElement('span');
            newElem0.className = 'badge dimbadge2 pull-right mob16';
            newElem0.id = 'remdip';
            newElem0.style = "cursor: pointer; font-weight: normal; padding: 10px; margin-top: 50px; margin-bottom: 10px; margin-right: 15px; border-radius: 30px";
            newElem0.setAttribute("onclick", "dimdip(this.id);");
            var text0 = document.createTextNode('\u2A09 Diminuer');
            newElem0.appendChild(text0);
            document.getElementById("myshowdiv2").appendChild(newElem0);
            var dimbadge = document.getElementsByClassName('dimbadge2');
            for (var i = 0; i < dimbadge.length; i++) {
                dimbadge[i].id = "remdip" + (i + 1);
            }

            var newElem1 = document.createElement('div');
            newElem1.className = 'form-group col-lg-6 mob17';
            newElem1.style = 'margin-top: 92px';
            var newElem2 = document.createElement('input');
            newElem2.className = 'form-control input-lg dipetabox';
            newElem2.setAttribute("name", "setablissement");
            newElem2.setAttribute("placeholder", "Etablissement *");
            newElem1.appendChild(newElem2);
            document.getElementById("myshowdiv2").appendChild(newElem1);

            var newElem3 = document.createElement('div');
            newElem3.className = 'form-group col-lg-6';
            newElem3.style = 'margin-top: 0px';
            var newElem4 = document.createElement('input');
            newElem4.className = 'form-control input-lg dipdipbox';
            newElem4.setAttribute("type", "text");
            newElem4.setAttribute("name", "sdiplome");
            newElem4.setAttribute("placeholder", "Diplome *");
            newElem3.appendChild(newElem4);
            document.getElementById("myshowdiv2").appendChild(newElem3);

            var newElem5 = document.createElement('div');
            newElem5.className = 'col-lg-6';
            newElem5.style = 'margin-top: 0px';
            var newElem6 = document.createElement('input');
            newElem6.className = 'form-control input-lg dipdatedebbox';
            newElem6.setAttribute("type", "date");
            newElem6.setAttribute("name", "sdatedebutetu");
            newElem6.style = 'margin-top: 20px; padding: 0; padding-left: 10px; padding-right: 10px; font-size: 13px';
            newElem5.appendChild(newElem6);
            document.getElementById("myshowdiv2").appendChild(newElem5);

            var newElem7 = document.createElement('div');
            newElem7.className = 'col-lg-6';
            newElem7.style = 'margin-top: 0px';
            var newElem8 = document.createElement('input');
            newElem8.className = 'form-control input-lg dipdatefinbox';
            newElem8.setAttribute("type", "date");
            newElem8.setAttribute("name", "sdatefinetu");
            newElem8.style = 'margin-top: 20px; padding: 0; padding-left: 10px; padding-right: 10px; font-size: 13px';
            newElem7.appendChild(newElem8);
            document.getElementById("myshowdiv2").appendChild(newElem7);

            var dipelema = document.getElementsByClassName("dipetabox");
            var dipelemb = document.getElementsByClassName("dipdipbox");
            var dipelemc = document.getElementsByClassName("dipdatedebbox");
            var dipelemd = document.getElementsByClassName("dipdatefinbox");

            for (var i = 0; i < dipelema.length; i++) {
                dipelema[i].name = "setablissement" + (i + 1);
                dipelema[i].id = "etablissement" + (i + 1);

            }

            for (var i = 0; i < dipelemb.length; i++) {
                dipelemb[i].name = "sdiplome" + (i + 1);
                dipelemb[i].id = "diplome" + (i + 1);

            }

            for (var i = 0; i < dipelemc.length; i++) {
                dipelemc[i].name = "sdatedebutetu" + (i + 1);
                dipelemc[i].id = "datedebutetu" + (i + 1);


            }

            for (var i = 0; i < dipelemd.length; i++) {
                dipelemd[i].name = "sdatefinetu" + (i + 1);
                dipelemd[i].id = "datefinetu" + (i + 1);

            }
  
            document.getElementById("comptdip").value = i;
          }

          
          function dimdip(divid) {
            var getid = divid.substring(6, 7);
            document.getElementById("etablissement" + getid).parentNode.remove();
            document.getElementById("diplome" + getid).parentNode.remove();
            document.getElementById("datedebutetu" + getid).parentNode.remove();
            document.getElementById("datefinetu" + getid).parentNode.remove();
            document.getElementById(divid).remove();
          }
          
        </script>

        <div class="col-lg-12" align="right">
          <button type="button" class="btn btn-default" id="bouton1" onclick="adddip();" style="margin-top: 20px; background-color: #00b0f0 "><i class="fas fa-plus"></i> Ajouter un diplome</button>
        </div>
        

        <div class="form-group col-lg-12 mob14" style="margin-top: 30px">
          <h4 style="font-weight: bold; border-left: solid 5px #00b0f0; padding-left: 10px; color: #00b0f0">COMPETENCES</h4>
        </div>

        <div class="col-lg-12 mob15" id="myshowdiv3" style="margin-top: -50px; padding: 0">
          <?php
            if (empty($row4)) {
              ?>
                <div class="form-group col-lg-6" style="margin-top: 50px">
                  <input class="form-control input-lg compdescboxold" type="text" name="sdescription_compold1" id="description_compold1" placeholder="Description *">
                </div>
                <div class="form-group col-lg-6" style="margin-top: 50px">
                  <select class="form-control input-lg compnivboxold" name="snivcompold1" id="nivcompold1">
                    <option selected disabled>Niveau</option>
                    <option value="0">Passable</option>
                    <option value="1">Assez-bien</option>
                    <option value="2">Bien</option>
                    <option value="3">Excélent</option>
                  </select>
                </div>
                <input type="hidden" id="comptcompold" name="scompcompteold" value="1">
              <?php
            }else{
              $i = 0;
              foreach ($row4 as $value4) {
                $i = $i + 1;
                ?>
                <input type="hidden" name="exist_comp<?php echo $i ?>" value="<?php echo $value4['id_comp'] ?>">
                  <a href="delete_field.php?id_field=<?php echo $value4['id_comp'] ?>&table=competence"><span class="badge pull-right mob16" style="cursor: pointer; font-weight: normal; padding: 10px; margin-top: 50px; margin-bottom: 10px; margin-right: 15px; border-radius: 30px">
                    <i class="fas fa-trash-alt"></i> Supprimer
                  </span></a>
                  <div class="form-group col-lg-6 mob17" style="margin-top: 92px">
                    <input class="form-control input-lg compdescboxold" type="text" name="sdescription_compold<?php echo $i ?>" id="description_compold<?php echo $i ?>" placeholder="Description *" value="<?php echo $value4['description'] ?>">
                  </div>
                  <div class="form-group col-lg-6">
                    <select class="form-control input-lg compnivboxold" name="snivcompold<?php echo $i ?>" id="nivcompold<?php echo $i ?>">
                      <?php
                        switch ($value4['niveau']) {

                          case 0:
                            ?>
                              <option disabled>Niveau</option>
                              <option selected value="0">Passable</option>
                              <option value="1">Assez-bien</option>
                              <option value="2">Bien</option>
                              <option value="3">Excélent</option>
                            <?php
                            break;

                            case 1:
                            ?>
                              <option disabled>Niveau</option>
                              <option value="0">Passable</option>
                              <option selected value="1">Assez-bien</option>
                              <option value="2">Bien</option>
                              <option value="3">Excélent</option>
                            <?php
                            break;

                            case 2:
                            ?>
                              <option disabled>Niveau</option>
                              <option value="0">Passable</option>
                              <option value="1">Assez-bien</option>
                              <option selected value="2">Bien</option>
                              <option value="3">Excélent</option>
                            <?php
                            break;

                            case 3:
                            ?>
                              <option disabled>Niveau</option>
                              <option value="0">Passable</option>
                              <option value="1">Assez-bien</option>
                              <option value="2">Bien</option>
                              <option selected value="3">Excélent</option>
                            <?php
                            break;
                          
                          default:
                            ?>
                              <option selected disabled>Niveau</option>
                              <option value="0">Passable</option>
                              <option value="1">Assez-bien</option>
                              <option value="2">Bien</option>
                              <option value="3">Excélent</option>
                            <?php
                            break;
                        }
                      ?>
                      
                    </select>
                  </div>
                <?php
              }
              ?>
                <input type="hidden" id="comptcompold" name="scompcompteold" value="<?php echo $i ?>">
              <?php
            }
          ?>
          

          <input type="hidden" id="comptcomp" name="scompcompte" value="0">
        </div>

        <script type="text/javascript">
          function addcomp() {

            var newElem0 = document.createElement('span');
            newElem0.className = 'badge dimbadge3 pull-right mob16';
            newElem0.id = 'remcomp';
            newElem0.style = "cursor: pointer; font-weight: normal; padding: 10px; margin-top: 50px; margin-bottom: 10px; margin-right: 15px; border-radius: 30px";
            newElem0.setAttribute("onclick", "dimcomp(this.id);");
            var text0 = document.createTextNode('\u2A09 Diminuer');
            newElem0.appendChild(text0);
            document.getElementById("myshowdiv3").appendChild(newElem0);
            var dimbadge = document.getElementsByClassName('dimbadge3');
            for (var i = 0; i < dimbadge.length; i++) {
                dimbadge[i].id = "remcomp" + (i + 1);
            }

            var newElem1 = document.createElement('div');
            newElem1.className = 'form-group col-lg-6 mob17';
            newElem1.style = 'margin-top: 92px';
            var newElem2 = document.createElement('input');
            newElem2.className = 'form-control input-lg compdescbox';
            newElem2.setAttribute("name", "sdescription_comp");
            newElem2.setAttribute("placeholder", "Description *");
            newElem1.appendChild(newElem2);
            document.getElementById("myshowdiv3").appendChild(newElem1);

            var newElem3 = document.createElement('div');
            newElem3.className = 'form-group col-lg-6';
            newElem3.style = 'margin-top: 0px';
            var newElem4 = document.createElement('select');
            newElem4.className = 'form-control input-lg compnivbox';
            newElem4.setAttribute("name", "snivcomp");
            var firstoption = document.createElement('option');
            firstoption.disabled = true;
            firstoption.selected = true;
            firstoption.text = 'Niveau';
            newElem4.appendChild(firstoption);
            //Create array of options to be added
            var array = ["Passable","Assez-bien","Bien","Excélent"];   
            //Create and append the options
            for (var i = 0; i < array.length; i++) {
                var option = document.createElement("option");
                option.value = i;
                option.text = array[i];
                newElem4.appendChild(option);
            }
            newElem3.appendChild(newElem4);
            document.getElementById("myshowdiv3").appendChild(newElem3);

            var compelema = document.getElementsByClassName("compdescbox");
            var compelemb = document.getElementsByClassName("compnivbox");
           

            for (var i = 0; i < compelema.length; i++) {
                compelema[i].name = "sdescription_comp" + (i + 1);
                compelema[i].id = "description_comp" + (i + 1);

            }

            for (var i = 0; i < compelemb.length; i++) {
                compelemb[i].name = "snivcomp" + (i + 1);
                compelemb[i].id = "nivcomp" + (i + 1);

            }
  
            document.getElementById("comptcomp").value = i;

            
          }

          function dimcomp(divid) {
            var getid = divid.substring(7, 8);
            document.getElementById("description_comp" + getid).parentNode.remove();
            document.getElementById("nivcomp" + getid).parentNode.remove();
            document.getElementById(divid).remove();
          }
        </script>

        <div class="col-lg-12" align="right">
          <button type="button" class="btn btn-default" id="bouton1" onclick="addcomp();" style="margin-top: 20px; background-color: #00b0f0 "><i class="fas fa-plus"></i> Ajouter une competence</button>
        </div>
        <div class="form-group col-lg-12 mob14" style="margin-top: 30px">
          <h4 style="font-weight: bold; border-left: solid 5px #00b0f0; padding-left: 10px; color: #00b0f0">LANGUES</h4>
        </div>

        <div class="col-lg-12 mob15" id="myshowdiv4" style="padding: 0; margin-top: -50px">
          <?php
            if (empty($row5)) {
              ?>
                <div class="form-group col-lg-6" style="margin-top: 50px">
                  <input class="form-control input-lg langdenboxold" type="text" name="snomlangold1" id="nomlangold1" placeholder="Dénomination *">
                </div>
                <div class="form-group col-lg-6" style="margin-top: 50px">
                  <select class="form-control input-lg langnivboxold" name="snivlangold1" id="nivlangold1">
                    <option selected disabled>Niveau</option>
                    <option value="0">Passable</option>
                    <option value="1">Assez-bien</option>
                    <option value="2">Bien</option>
                    <option value="3">Excélent</option>
                  </select>
                </div>

                <input type="hidden" id="comptlangold" name="slangcompteold" value="1">
              <?php
            }else{
              $i = 0;
              foreach ($row5 as $value5) {
                $i = $i + 1;
                ?>
                  <input type="hidden" name="exist_lang<?php echo $i ?>" value="<?php echo $value5['id_lang'] ?>">
                  <a href="delete_field.php?id_field=<?php echo $value5['id_lang'] ?>&table=langue"><span class="badge pull-right mob16" style="cursor: pointer; font-weight: normal; padding: 10px; margin-top: 50px; margin-bottom: 10px; margin-right: 15px; border-radius: 30px">
                    <i class="fas fa-trash-alt"></i> Supprimer
                  </span></a>
                  <div class="form-group col-lg-6 mob17" style="margin-top: 92px">
                    <input class="form-control input-lg langdenboxold" type="text" name="snomlangold<?php echo $i ?>" id="nomlangold<?php echo $i ?>" placeholder="Dénomination *" value="<?php echo $value5['denomiation'] ?>">
                  </div>
                  <div class="form-group col-lg-6">

                    <select class="form-control input-lg langnivboxold" name="snivlangold<?php echo $i ?>" id="nivlangold<?php echo $i ?>">
                      <?php
                      switch ($value5['niveau']) {
                        case 0:
                          ?>
                            <option disabled>Niveau</option>
                            <option selected value="0">Passable</option>
                            <option value="1">Assez-bien</option>
                            <option value="2">Bien</option>
                            <option value="3">Excélent</option>
                          <?php
                          break;

                          case 1:
                          ?>
                            <option disabled>Niveau</option>
                            <option value="0">Passable</option>
                            <option selected value="1">Assez-bien</option>
                            <option value="2">Bien</option>
                            <option value="3">Excélent</option>
                          <?php
                          break;

                          case 2:
                          ?>
                            <option disabled>Niveau</option>
                            <option value="0">Passable</option>
                            <option value="1">Assez-bien</option>
                            <option selected value="2">Bien</option>
                            <option value="3">Excélent</option>
                          <?php
                          break;

                          case 3:
                          ?>
                            <option disabled>Niveau</option>
                            <option value="0">Passable</option>
                            <option value="1">Assez-bien</option>
                            <option value="2">Bien</option>
                            <option selected value="3">Excélent</option>
                          <?php
                          break;
                        
                        default:
                          ?>
                            <option selected disabled>Niveau</option>
                            <option value="0">Passable</option>
                            <option value="1">Assez-bien</option>
                            <option value="2">Bien</option>
                            <option value="3">Excélent</option>
                          <?php
                          break;
                      }
                    ?>
                    </select>
                  </div>
                <?php
              }
              ?>
                <input type="hidden" id="comptlangold" name="slangcompteold" value="<?php echo $i ?>">
              <?php
            }
          ?>
                
              
          <input type="hidden" id="comptlang" name="slangcompte" value="0">
        </div>

        <script type="text/javascript">
          function addlang() {

            var newElem0 = document.createElement('span');
            newElem0.className = 'badge dimbadge4 pull-right mob16';
            newElem0.id = 'remlang';
            newElem0.style = "cursor: pointer; font-weight: normal; padding: 10px; margin-top: 50px; margin-bottom: 10px; margin-right: 15px; border-radius: 30px";
            newElem0.setAttribute("onclick", "dimlang(this.id);");
            var text0 = document.createTextNode('\u2A09 Diminuer');
            newElem0.appendChild(text0);
            document.getElementById("myshowdiv4").appendChild(newElem0);
            var dimbadge = document.getElementsByClassName('dimbadge4');
            for (var i = 0; i < dimbadge.length; i++) {
                dimbadge[i].id = "remlang" + (i + 1);
            }

            var newElem1 = document.createElement('div');
            newElem1.className = 'form-group col-lg-6 mob17';
            newElem1.style = 'margin-top: 92px';
            var newElem2 = document.createElement('input');
            newElem2.className = 'form-control input-lg langdenbox';
            newElem2.setAttribute("name", "snomlang");
            newElem2.setAttribute("placeholder", "Dénomination *");
            newElem1.appendChild(newElem2);
            document.getElementById("myshowdiv4").appendChild(newElem1);

            var newElem3 = document.createElement('div');
            newElem3.className = 'form-group col-lg-6';
            newElem3.style = 'margin-top: 0px';
            var newElem4 = document.createElement('select');
            newElem4.className = 'form-control input-lg langnivbox';
            newElem4.setAttribute("name", "snivlang");
            var firstoption = document.createElement('option');
            firstoption.disabled = true;
            firstoption.selected = true;
            firstoption.text = 'Niveau';
            newElem4.appendChild(firstoption);
            //Create array of options to be added
            var array = ["Passable","Assez-bien","Bien","Excélent"];   
            //Create and append the options
            for (var i = 0; i < array.length; i++) {
                var option = document.createElement("option");
                option.value = i;
                option.text = array[i];
                newElem4.appendChild(option);
            }
            newElem3.appendChild(newElem4);
            document.getElementById("myshowdiv4").appendChild(newElem3);

            var langelema = document.getElementsByClassName("langdenbox");
            var langelemb = document.getElementsByClassName("langnivbox");
           
            for (var i = 0; i < langelema.length; i++) {
                langelema[i].name = "snomlang" + (i + 1);
                langelema[i].id = "nomlang" + (i + 1);

            }

            for (var i = 0; i < langelemb.length; i++) {
                langelemb[i].name = "snivlang" + (i + 1);
                langelemb[i].id = "nivlang" + (i + 1);

            }
  
            document.getElementById("comptlang").value = i;
          }

          function dimlang(divid) {
            var getid = divid.substring(7, 8);
            document.getElementById("nomlang" + getid).parentNode.remove();
            document.getElementById("nivlang" + getid).parentNode.remove();
            document.getElementById(divid).remove();
            // alert(divid);
          }
        </script>

        <div class="col-lg-12" align="right">
          <button type="button" class="btn btn-default" id="bouton1" onclick="addlang();" style="margin-top: 20px; background-color: #00b0f0 "><i class="fas fa-plus"></i> Ajouter une langue</button>
        </div>
        <div class="form-group col-lg-12 mob14" style="margin-top: 30px">
          <h4 style="font-weight: bold; border-left: solid 5px #00b0f0; padding-left: 10px; color: #00b0f0">SPORT</h4>
        </div>

        <div class="col-lg-12 mob15" id="myshowdiv5" style="padding: 0; margin-top: -50px">
          <?php
            if (empty($row6)) {
              ?>
                <div class="form-group col-lg-6" style="margin-top: 50px">
                  <input class="form-control input-lg sponomboxold" type="text" name="snomsportold1" id="nomsportold1" placeholder="Sport pratiqué *">
                </div>
                <div class="form-group col-lg-6" style="margin-top: 50px">
                  <select class="form-control input-lg sponivboxold" name="snivsportold1" id="nivsportold1">
                    <option selected disabled>Niveau</option>
                    <option value="0">Passable</option>
                    <option value="1">Assez-bien</option>
                    <option value="2">Bien</option>
                    <option value="3">Excélent</option>
                  </select>
                </div>
                <input type="hidden" id="comptspoold" name="sspocompteold" value="1">
              <?php
            }else{
              $i = 0;
              foreach ($row6 as $value6) {
                $i = $i + 1;
                ?>
                <input type="hidden" name="exist_spo<?php echo $i ?>" value="<?php echo $value6['id_sport'] ?>">
                <a href="delete_field.php?id_field=<?php echo $value6['id_sport'] ?>&table=sport"><span class="badge pull-right mob16" style="cursor: pointer; font-weight: normal; padding: 10px; margin-top: 50px; margin-bottom: 10px; margin-right: 15px; border-radius: 30px">
                    <i class="fas fa-trash-alt"></i> Supprimer
                  </span></a>
                <div class="form-group col-lg-6 mob17" style="margin-top: 92px">
                  <input class="form-control input-lg sponomboxold" type="text" name="snomsportold<?php echo $i ?>" id="nomsportold<?php echo $i ?>" placeholder="Sport pratiqué *" value="<?php echo $value6['nom_sport'] ?>">
                </div>
                <div class="form-group col-lg-6">
                  <select class="form-control input-lg sponivboxold" name="snivsportold<?php echo $i ?>" id="nivsportold<?php echo $i ?>">
                    <?php
                      switch ($value6['niveau']) {
                        case 0:
                          ?>
                            <option disabled>Niveau</option>
                            <option selected value="0">Passable</option>
                            <option value="1">Assez-bien</option>
                            <option value="2">Bien</option>
                            <option value="3">Excélent</option>
                          <?php
                          break;

                          case 1:
                          ?>
                            <option disabled>Niveau</option>
                            <option value="0">Passable</option>
                            <option selected value="1">Assez-bien</option>
                            <option value="2">Bien</option>
                            <option value="3">Excélent</option>
                          <?php
                          break;

                          case 2:
                          ?>
                            <option disabled>Niveau</option>
                            <option value="0">Passable</option>
                            <option value="1">Assez-bien</option>
                            <option selected value="2">Bien</option>
                            <option value="3">Excélent</option>
                          <?php
                          break;

                          case 3:
                          ?>
                            <option disabled>Niveau</option>
                            <option value="0">Passable</option>
                            <option value="1">Assez-bien</option>
                            <option value="2">Bien</option>
                            <option selected value="3">Excélent</option>
                          <?php
                          break;
                        
                        default:
                          ?>
                            <option selected disabled>Niveau</option>
                            <option value="0">Passable</option>
                            <option value="1">Assez-bien</option>
                            <option value="2">Bien</option>
                            <option value="3">Excélent</option>
                          <?php
                          break;
                      }
                    ?>
                  </select>
                </div>
                <?php
              }
              ?>
                <input type="hidden" id="comptspoold" name="sspocompteold" value="<?php echo $i ?>">
              <?php
            }
          ?>
          

          <input type="hidden" id="comptspo" name="sspocompte" value="0">
        </div>

        <script type="text/javascript">
          function addspo() {

            var newElem0 = document.createElement('span');
            newElem0.className = 'badge dimbadge5 pull-right mob16';
            newElem0.id = 'remspo';
            newElem0.style = "cursor: pointer; font-weight: normal; padding: 10px; margin-top: 50px; margin-bottom: 10px; margin-right: 15px; border-radius: 30px";
            newElem0.setAttribute("onclick", "dimspo(this.id);");
            var text0 = document.createTextNode('\u2A09 Diminuer');
            newElem0.appendChild(text0);
            document.getElementById("myshowdiv5").appendChild(newElem0);
            var dimbadge = document.getElementsByClassName('dimbadge5');
            for (var i = 0; i < dimbadge.length; i++) {
                dimbadge[i].id = "remspo" + (i + 1);
            }

            var newElem1 = document.createElement('div');
            newElem1.className = 'form-group col-lg-6 mob17';
            newElem1.style = 'margin-top: 92px';
            var newElem2 = document.createElement('input');
            newElem2.className = 'form-control input-lg sponombox';
            newElem2.setAttribute("name", "snomsport");
            newElem2.setAttribute("placeholder", "Sport pratiqué *");
            newElem1.appendChild(newElem2);
            document.getElementById("myshowdiv5").appendChild(newElem1);

            var newElem3 = document.createElement('div');
            newElem3.className = 'form-group col-lg-6';
            newElem3.style = 'margin-top: 0px';
            var newElem4 = document.createElement('select');
            newElem4.className = 'form-control input-lg sponivbox';
            newElem4.setAttribute("name", "snivsport");
            var firstoption = document.createElement('option');
            firstoption.disabled = true;
            firstoption.selected = true;
            firstoption.text = 'Niveau';
            newElem4.appendChild(firstoption);
            //Create array of options to be added
            var array = ["Passable","Assez-bien","Bien","Excélent"];   
            //Create and append the options
            for (var i = 0; i < array.length; i++) {
                var option = document.createElement("option");
                option.value = i;
                option.text = array[i];
                newElem4.appendChild(option);
            }
            newElem3.appendChild(newElem4);
            document.getElementById("myshowdiv5").appendChild(newElem3);

            var spoelema = document.getElementsByClassName("sponombox");
            var spoelemb = document.getElementsByClassName("sponivbox");
           

            for (var i = 0; i < spoelema.length; i++) {
                spoelema[i].name = "snomsport" + (i + 1);
                spoelema[i].id = "nomsport" + (i + 1);

            }

            for (var i = 0; i < spoelemb.length; i++) {
                spoelemb[i].name = "snivsport" + (i + 1);
                spoelemb[i].id = "nivsport" + (i + 1);

            }
  
            document.getElementById("comptspo").value = i;
          }

          function dimspo(divid) {
            var getid = divid.substring(6, 7);
            document.getElementById("nomsport" + getid).parentNode.remove();
            document.getElementById("nivsport" + getid).parentNode.remove();
            document.getElementById(divid).remove();
          }
        </script>

        <div class="col-lg-12" align="right">
          <button type="button" class="btn btn-default" id="bouton1" onclick="addspo();" style="margin-top: 20px; background-color: #00b0f0 "><i class="fas fa-plus"></i> Ajouter un sport</button>
        </div>

        <div class="form-group col-lg-12 mob14" style="margin-top: 30px">
          <h4 style="font-weight: bold; border-left: solid 5px #00b0f0; padding-left: 10px; color: #00b0f0">AUTRES ACTIVITES</h4>
        </div>

        <div class="col-lg-12 mob15" id="myshowdiv6" style="padding: 0; margin-top: -50px">
          <?php
            if (empty($row7)) {
              ?>
                <div class="form-group col-lg-6" style="margin-top: 50px">
                  <input class="form-control input-lg autacnomboxold" type="text" name="snomactiviteold1" id="nomactiviteold1" placeholder="Activités *">
                </div>
                <div class="form-group col-lg-6" style="margin-top: 50px">
                  <select class="form-control input-lg autacnivboxold" name="snivactivold1" id="nivactivold1">
                    <option selected disabled>Niveau</option>
                    <option value="0">Passable</option>
                    <option value="1">Assez-bien</option>
                    <option value="2">Bien</option>
                    <option value="3">Excélent</option>
                  </select>
                </div>
                <input type="hidden" id="comptautacold" name="sautaccompteold" value="1">
              <?php
            }else{
              $i = 0;
              foreach ($row7 as $value7) {
                $i = $i + 1; 
                ?>
                  <input type="hidden" name="exist_activ<?php echo $i ?>" value="<?php echo $value7['id_activ'] ?>">
                  <a href="delete_field.php?id_field=<?php echo $value7['id_activ'] ?>&table=activites"><span class="badge pull-right mob16" style="cursor: pointer; font-weight: normal; padding: 10px; margin-top: 50px; margin-bottom: 10px; margin-right: 15px; border-radius: 30px">
                    <i class="fas fa-trash-alt"></i> Supprimer
                  </span></a>
                  <div class="form-group col-lg-6 mob17" style="margin-top: 92px">
                    <input class="form-control input-lg autacnomboxold" type="text" name="snomactiviteold<?php echo $i ?>" id="nomactiviteold<?php echo $i ?>" placeholder="Activités *" value="<?php echo $value7['nom_activ'] ?>">
                  </div>
                  <div class="form-group col-lg-6">
                    <select class="form-control input-lg autacnivboxold" name="snivactivold<?php echo $i ?>" id="nivactivold<?php echo $i ?>">
                      <?php
                        switch ($value7['niv_activ']) {
                          case 0:
                            ?>
                              <option disabled>Niveau</option>
                              <option selected value="0">Passable</option>
                              <option value="1">Assez-bien</option>
                              <option value="2">Bien</option>
                              <option value="3">Excélent</option>
                            <?php
                            break;

                            case 1:
                            ?>
                              <option disabled>Niveau</option>
                              <option value="0">Passable</option>
                              <option selected value="1">Assez-bien</option>
                              <option value="2">Bien</option>
                              <option value="3">Excélent</option>
                            <?php
                            break;

                            case 2:
                            ?>
                              <option disabled>Niveau</option>
                              <option value="0">Passable</option>
                              <option value="1">Assez-bien</option>
                              <option selected value="2">Bien</option>
                              <option value="3">Excélent</option>
                            <?php
                            break;

                            case 3:
                            ?>
                              <option disabled>Niveau</option>
                              <option value="0">Passable</option>
                              <option value="1">Assez-bien</option>
                              <option value="2">Bien</option>
                              <option selected value="3">Excélent</option>
                            <?php
                            break;
                          
                          default:
                            ?>
                              <option selected disabled>Niveau</option>
                              <option value="0">Passable</option>
                              <option value="1">Assez-bien</option>
                              <option value="2">Bien</option>
                              <option value="3">Excélent</option>
                            <?php
                            break;
                        }
                      ?>
                      
                    </select>
                  </div>
                <?php
              }
              ?>
                <input type="hidden" id="comptautacold" name="sautaccompteold" value="<?php echo $i ?>">
              <?php
            }
          ?>
          

          <input type="hidden" id="comptautac" name="sautaccompte" value="0">
        </div>

        <script type="text/javascript">
          function addautac() {

            var newElem0 = document.createElement('span');
            newElem0.className = 'badge dimbadge6 pull-right mob16';
            newElem0.id = 'remautac';
            newElem0.style = "cursor: pointer; font-weight: normal; padding: 10px; margin-top: 50px; margin-bottom: 10px; margin-right: 15px; border-radius: 30px";
            newElem0.setAttribute("onclick", "dimautac(this.id);");
            var text0 = document.createTextNode('\u2A09 Diminuer');
            newElem0.appendChild(text0);
            document.getElementById("myshowdiv6").appendChild(newElem0);
            var dimbadge = document.getElementsByClassName('dimbadge6');
            for (var i = 0; i < dimbadge.length; i++) {
                dimbadge[i].id = "remautac" + (i + 1);
            }

            var newElem1 = document.createElement('div');
            newElem1.className = 'form-group col-lg-6 mob17';
            newElem1.style = 'margin-top: 92px';
            var newElem2 = document.createElement('input');
            newElem2.className = 'form-control input-lg autacnombox';
            newElem2.setAttribute("name", "snomactivite");
            newElem2.setAttribute("placeholder", "Activités *");
            newElem1.appendChild(newElem2);
            document.getElementById("myshowdiv6").appendChild(newElem1);

            var newElem3 = document.createElement('div');
            newElem3.className = 'form-group col-lg-6';
            newElem3.style = 'margin-top: 0px';
            var newElem4 = document.createElement('select');
            newElem4.className = 'form-control input-lg autacnivbox';
            newElem4.setAttribute("name", "snivactiv");
            var firstoption = document.createElement('option');
            firstoption.disabled = true;
            firstoption.selected = true;
            firstoption.text = 'Niveau';
            newElem4.appendChild(firstoption);
            //Create array of options to be added
            var array = ["Passable","Assez-bien","Bien","Excélent"];   
            //Create and append the options
            for (var i = 0; i < array.length; i++) {
                var option = document.createElement("option");
                option.value = i;
                option.text = array[i];
                newElem4.appendChild(option);
            }
            newElem3.appendChild(newElem4);
            document.getElementById("myshowdiv6").appendChild(newElem3);

            var autacelema = document.getElementsByClassName("autacnombox");
            var autacelemb = document.getElementsByClassName("autacnivbox");
           

            for (var i = 0; i < autacelema.length; i++) {
                autacelema[i].name = "snomactivite" + (i + 1);
                autacelema[i].id = "nomactivite" + (i + 1);

            }

            for (var i = 0; i < autacelemb.length; i++) {
                autacelemb[i].name = "snivactiv" + (i + 1);
                autacelemb[i].id = "nivactiv" + (i + 1);

            }
  
            document.getElementById("comptautac").value = i;
          }

          function dimautac(divid) {
            var getid = divid.substring(8, 9);
            document.getElementById("nomactivite" + getid).parentNode.remove();
            document.getElementById("nivactiv" + getid).parentNode.remove();
            document.getElementById(divid).remove();
          }
        </script>

        <div class="col-lg-12" align="right">
          <button type="button" class="btn btn-default" id="bouton1" onclick="addautac();" style="margin-top: 20px; background-color: #00b0f0 "><i class="fas fa-plus"></i> Ajouter une activite</button>
        </div>

          <div class="col-lg-12" align="center" style="margin-top: 30px">
            <button type="button" class="btn btn-default" id="bouton2" onclick="submitForm()"><i class="fas fa-check"></i></button>
          </div>
      </form>

      <script type="text/javascript">
        function submitForm() {
         var frm = document.getElementsByName('myform')[0];
         frm.submit(); // Submit the form
         frm.reset();  // Reset all form data
        }
      </script>
      
    </div>
  
     
</body>
      
</html>